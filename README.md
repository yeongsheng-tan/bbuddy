BBuddy
===

## Installation
Please install the following tools for this project. The latest version should be fine unless specific version is listed.

On development box, Mac OSX machine, ensure homebrew is setup
- `brew cask install virtualbox`
- `brew cask install vagrant`

In target VM, install/setup and configure the following
- JDK 1.8.x
- Gradle 2.13
- MySQL 5.x
  - database: bbuddydev
  - username: nerd
  - password: dbs3cr3t
- Tomcat 8.5.x
