# TODO
# 1)  Edit file 'provision.sh' to build and deploy BBuddy java web app automatically
# 2)  Setup jdk8 (see http://openjdk.java.net/install/)
# 3)  Setup gradle 2.13 into /opt (see https://gradle.org/install/)
# 4)  Setup MySQL Server 5.x
# 5)  Create MySQL DB user 'nerd' with password 'dbs3cr3t' and database 'bbuddydev' for 'localhost' access
# 6)  Setup apache-tomcat 8.5.15 into /opt
# 7)  Copy app java source into target VM into a temp dir and compile using gradle (hint: requires build DSL build.gradle & gradle dir from source bundle)
# 8)  Setup apache-tomcat 8.5.27 into /opt (see https://www-eu.apache.org/dist/tomcat/)
# 9)  Deploy compiled war from step (7) build/libs/bbuddy-HEAD.war into webapp server as tomcat ROOT webapp
# 10) Start tomcat and check server is up and running with app w/o issues
# 11) Test app using curl to http://localhost:8080 -> Expect HTTP 302 response with redirect to http://localhost:8080/signin
printf "Dev\u00D8\u20BD\u2233 \u21e8 Let's start...\n"
